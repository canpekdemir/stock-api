# J1 - Payconiq Stock API coding task

The project provides a RESTful API to manage the stock resource

For the backend an embedded H2 Database and an embedded Redis are used.
 Project data is initialized upon application start-up using the
*data.sql*

## Important notes about project

>  Restful api standards and versioning patterns are applied while designing restful resources.

>  DDD and Hexagonal architecture(ports & adapters) were applied for packaging style and extracted the adapters from the domain layer to the infrastructure layer.

>  I tried to fully coverage of the project with unit and integration tests.
JUnit, Mockito and testRestTemplate were used.

>  Circuit breaker pattern was applied to prevent the exploding of number of threads. Spring cloud hystrix was used to implement this while retrieving stock list in StockService.
A fallback method(reliableStockList) was implemented to return dummy or stale data.

>  Spring-retry was also used while creating new stock. The createStock method retries 3 times to complete successfully.
  
>  For caching the stock list and stock item, spring cacheable annotation used and redis is used for the caching choice.
This is configured in application.yml as cache.type 

>  LockService in Redis was implemented to prevent double-click created duplicate stock records. This is applied in CREATE(POST) request.

>  A helper class XSSCheckUtils was implemented to prevent xss attacks. While returning stock items, stockNames are cleared with this class.

>  Logging and correlationId interceptors(LogExecutionInterceptor & CorrelationIdInterceptor) were implemented to log and trace the request in a whole process.
Every request started and ended logs are written with correlationId.

>  RestControllerExceptionHandler was implemented to catch validation and business runtime exceptions.
The other generic exceptions are also caught to handle spring-generated error responses.

>  Hikari was used to implement connection pool.

>  LocalizationConfiguration added to localize error messages with EN or TR languages.

>  logback used for logging.

## RESTFUL API

The API provides the following interfaces

### Create Stock
**POST /api/v1/stocks**

Request:
```
Headers:
Accept: application/json
Content-Type: application/json
Body: {
      	"name":"stock124",
      	"currentPrice": 120.50
      }
```

Create stock response:
```
Code: 201
Body: {
          "status": "success",
          "systemTime": 1519803952126
      }
```

### Update stock
**PUT /api/v1/stocks/{stockId}**

Request:
```
Headers: Accept: application/json, Content-Type: application/json
Body: {
      	"name":"STOCK1111",
      	"currentPrice":121.00
      }
```

Update stock response:
```
Code: 200
Body: {
          "status": "success",
          "systemTime": 1519804111335
      }
```

### Get all stocks
**GET /api/v1/stocks?page=1&count=5**

Request:
```
Headers:
Accept: application/json
```
success response:
```
Code: 200
Body: {
          "status": "success",
          "systemTime": 1519804176478,
          "stocks": [
              {
                  "id": 1,
                  "name": "STOCK1",
                  "currentPrice": 1
              },
              {
                  "id": 2,
                  "name": "STOCK2",
                  "currentPrice": 1.5
              },
              {
                  "id": 3,
                  "name": "STOCK3",
                  "currentPrice": 1.7
              },
              {
                  "id": 4,
                  "name": "STOCK4",
                  "currentPrice": 2
              },
              {
                  "id": 5,
                  "name": "STOCK5",
                  "currentPrice": 0.5
              }
          ]
      }
```

### Get only one stock
**GET /api/v1/stocks/{stockId}**

request:
```
Headers: Accept: application/json
```
success response:
```
Code: 200
Body: {
          "status": "success",
          "systemTime": 1519804230673,
          "stock": {
              "id": 5,
              "name": "STOCK5",
              "currentPrice": 0.5
          }
      }
```



