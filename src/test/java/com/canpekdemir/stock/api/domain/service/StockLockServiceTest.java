package com.canpekdemir.stock.api.domain.service;


import com.canpekdemir.stock.api.domain.exception.LockedException;
import com.canpekdemir.stock.api.domain.repository.LockRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StockLockServiceTest {

    @InjectMocks
    StockLockService stockLockService;

    @Mock
    LockRepository lockRepository;

    @Test(expected = LockedException.class)
    public void should_throw_exception_when_lock_count_is_bigger_than_one() throws Exception {
        //given
        String name = "stock1";

        //mock
        when(lockRepository.increment("stock:stock1", 5L, TimeUnit.SECONDS)).thenReturn(2);

        //when
        stockLockService.lock(name);
    }

    @Test
    public void should_not_throw_exception_when_lock_count_is_one() throws Exception {
        //given
        String name = "stock1";

        //mock
        when(lockRepository.increment("stock:stock1", 5L, TimeUnit.SECONDS)).thenReturn(1);

        //when
        Throwable throwable = catchThrowable(() -> stockLockService.lock(name));

        //then
        assertThat(throwable).isNull();
    }
}