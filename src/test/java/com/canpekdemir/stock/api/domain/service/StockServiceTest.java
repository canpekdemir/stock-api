package com.canpekdemir.stock.api.domain.service;

import com.canpekdemir.stock.api.domain.entity.Stock;
import com.canpekdemir.stock.api.domain.exception.StockAlreadyCreatedException;
import com.canpekdemir.stock.api.domain.exception.StockNotFoundException;
import com.canpekdemir.stock.api.domain.repository.StockRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StockServiceTest {

    @InjectMocks
    StockService stockService;

    @Mock
    StockRepository stockRepository;

    @Test
    public void should_retrieve_stocks_when_page_and_count_is_null() {
        //given
        Integer page = null;
        Integer count = null;

        //mock
        List<Stock> stocks = new ArrayList<>();
        Stock stock = new Stock();
        stocks.add(stock);
        Page<Stock> pageOfStocks = new PageImpl<>(stocks);
        when(stockRepository.findAll(any(PageRequest.class))).thenReturn(pageOfStocks);

        //then
        List<Stock> stockResultList = stockService.retrieveStocks(page, count);
        assertThat(stockResultList).hasSize(1);
    }

    @Test(expected = StockNotFoundException.class)
    public void should_throw_exception_when_stock_is_not_found() throws Exception {
        //given
        Long stockId = 1L;

        //mock
        when(stockRepository.findById(stockId)).thenReturn(Optional.empty());

        //when
        stockService.retrieveStock(stockId);
    }

    @Test
    public void should_retrieve_stock() {
        //given
        Long stockId = 1L;

        //mock
        Stock stock = new Stock();
        Optional<Stock> stockOptional = Optional.of(stock);
        when(stockRepository.findById(stockId)).thenReturn(stockOptional);

        //when
        Stock stockResult = stockService.retrieveStock(stockId);
        assertThat(stockResult).isNotNull();
    }

    @Test(expected = StockAlreadyCreatedException.class)
    public void should_throw_exception_when_stock_is_already_created() {
        //given
        String name = "stock1";
        BigDecimal currentPrice = new BigDecimal("10.0");

        //mock
        Optional<Stock> stockOptional = Optional.of(new Stock());
        when(stockRepository.findByName(name)).thenReturn(stockOptional);

        //when
        stockService.createStock(name,currentPrice);
    }

    @Test
    public void should_create_stock() {
        //given
        String name = "stock1";
        BigDecimal currentPrice = new BigDecimal("10.0");

        //mock
        when(stockRepository.findByName(name)).thenReturn(Optional.empty());

        //when
        stockService.createStock(name,currentPrice);

        //then
        verify(stockRepository,times(1)).save(any(Stock.class));
    }

    @Test
    public void should_update_stock() {
        //given
        Long stockId = 1L;
        String name = "stock1";
        BigDecimal currentPrice = new BigDecimal("10.0");

        //mock
        Optional<Stock> stockOptional = Optional.of(new Stock());

        when(stockRepository.findByName(name)).thenReturn(Optional.empty());
        when(stockRepository.findById(stockId)).thenReturn(stockOptional);

        //when
        stockService.updateStock(stockId,name,currentPrice);

        //then
        verify(stockRepository,times(1)).save(any(Stock.class));
    }
}