package com.canpekdemir.stock.api.infrastructure.rest;

import com.canpekdemir.stock.api.application.model.dto.StockDto;
import com.canpekdemir.stock.api.application.model.request.StockRequest;
import com.canpekdemir.stock.api.application.model.response.Response;
import com.canpekdemir.stock.api.application.model.response.ResponseStatusType;
import com.canpekdemir.stock.api.application.model.response.StockListResponse;
import com.canpekdemir.stock.api.application.model.response.StockResponse;
import com.canpekdemir.stock.api.domain.entity.Stock;
import com.canpekdemir.stock.api.domain.repository.StockRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UriComponentsBuilder;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RestStockControllerIT {

    @Autowired
    TestRestTemplate testRestTemplate;

    @Autowired
    StockRepository stockRepository;

    @Value("${local.server.port}")
    private int serverPort;

    @Test
    public void should_retrieve_all_stocks() {
        //given
        Integer page = 1;
        Integer count = 5;

        //when
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl("http://localhost:" + serverPort + "/api/v1/stocks")
                .queryParam("page", page)
                .queryParam("count", count);

        HttpHeaders headers = new HttpHeaders();
        ResponseEntity<StockListResponse> response = testRestTemplate.exchange(builder.build().encode().toUri(), HttpMethod.GET, new HttpEntity<>(headers), StockListResponse.class);

        //then
        StockListResponse responseBody = response.getBody();

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseBody.getStatus()).isEqualTo(ResponseStatusType.SUCCESS.getValue());

        List<StockDto> stocks = responseBody.getStocks();
        assertThat(stocks)
                .hasSize(5)
                .extracting("id", "name", "currentPrice")
                .contains(tuple(1L, "STOCK1", new BigDecimal("1.00")),
                        tuple(2L, "STOCK2", new BigDecimal("1.50")),
                        tuple(3L, "STOCK3", new BigDecimal("1.70")),
                        tuple(4L, "STOCK4", new BigDecimal("2.00")),
                        tuple(5L, "STOCK5", new BigDecimal("0.50")));
    }

    @Test
    public void should_retrieve_one_stock() {
        //given
        Long stockId = 1L;

        //when
        ResponseEntity<StockResponse> response = testRestTemplate
                .getForEntity("/api/v1/stocks/{stockId}", StockResponse.class, stockId);

        //then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        StockResponse body = response.getBody();
        StockDto stock = body.getStock();

        assertThat(body.getStatus()).isEqualTo(ResponseStatusType.SUCCESS.getValue());

        assertThat(stock.getId()).isEqualTo(1L);
        assertThat(stock.getName()).isEqualTo("STOCK1");
        assertThat(stock.getCurrentPrice()).isEqualTo(new BigDecimal("1.00"));
    }

    @Test
    public void should_create_stock() {
        //given
        StockRequest request = new StockRequest();
        request.setName("STOCK999");
        request.setCurrentPrice(new BigDecimal("99.99"));
        request.setLocale("en");

        //when
        ResponseEntity<?> response = testRestTemplate.postForEntity("/api/v1/stocks", request, Response.class);

        //then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);

        Optional<Stock> stock999 = stockRepository.findByName("STOCK999");
        assertThat(stock999.isPresent()).isTrue();

        Stock stock = stock999.get();
        assertThat(stock.getName()).isEqualTo("STOCK999");
        assertThat(stock.getCurrentPrice()).isEqualTo(new BigDecimal("99.99"));
    }

    @Test
    public void should_update_stock() {
        //given
        Long stockId = 10L;

        StockRequest request = new StockRequest();
        request.setName("STOCK100");
        request.setCurrentPrice(new BigDecimal("2.30"));
        request.setLocale("en");


        //when
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl("http://localhost:" + serverPort + "/api/v1/stocks/" + stockId);
        HttpHeaders headers = new HttpHeaders();
        ResponseEntity<?> response = testRestTemplate.exchange(builder.build().encode().toUri(), HttpMethod.PUT,new HttpEntity<>(request,headers), Response.class);

        //then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        Optional<Stock> stock100 = stockRepository.findByName("STOCK100");
        assertThat(stock100.isPresent()).isTrue();

        Stock stock = stock100.get();
        assertThat(stock.getName()).isEqualTo("STOCK100");
        assertThat(stock.getCurrentPrice()).isEqualTo(new BigDecimal("2.30"));
    }
}