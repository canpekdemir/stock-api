package com.canpekdemir.stock.api.application.converter;

import com.canpekdemir.stock.api.application.manager.mapper.ResponseMapper;
import com.canpekdemir.stock.api.application.model.dto.StockDto;
import com.canpekdemir.stock.api.application.model.response.StockListResponse;
import com.canpekdemir.stock.api.application.model.response.StockResponse;
import com.canpekdemir.stock.api.domain.entity.Stock;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;

public class StockResponseConverterTest {

    StockResponseConverter stockResponseConverter;

    @Before
    public void setUp() throws Exception {
        stockResponseConverter = new StockResponseConverter(new ResponseMapper());
    }

    @Test
    public void should_convert_stocks() {
        //given
        Stock stock = new Stock();
        stock.setId(1L);
        stock.setName("STOCK1");
        stock.setCurrentPrice(new BigDecimal("1.00"));

        List<Stock> stocks = new ArrayList<>();
        stocks.add(stock);

        //when
        StockListResponse response = stockResponseConverter.convertStocks(stocks);

        //then
        List<StockDto> responseStocks = response.getStocks();

        assertThat(responseStocks)
                .hasSize(1)
                .extracting("id", "name", "currentPrice")
                .contains(tuple(1L, "STOCK1", new BigDecimal("1.00")));
    }

    @Test
    public void should_convert_stock() {
        //given
        Stock stock = new Stock();
        stock.setId(1L);
        stock.setName("STOCK1");
        stock.setCurrentPrice(new BigDecimal("1.00"));

        //when
        StockResponse stockResponse = stockResponseConverter.convertStockToResponse(stock);

        //then
        StockDto stockDto = stockResponse.getStock();
        assertThat(stockDto.getId()).isEqualTo(1L);
        assertThat(stockDto.getName()).isEqualTo("STOCK1");
        assertThat(stockDto.getCurrentPrice()).isEqualTo(new BigDecimal("1.00"));
    }
}