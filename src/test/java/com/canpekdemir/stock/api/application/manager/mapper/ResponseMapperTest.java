package com.canpekdemir.stock.api.application.manager.mapper;

import com.canpekdemir.stock.api.application.model.response.Response;
import com.canpekdemir.stock.api.application.model.response.ResponseStatusType;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ResponseMapperTest {

    ResponseMapper responseMapper;

    @Before
    public void setUp() throws Exception {
        responseMapper = new ResponseMapper();
    }

    @Test
    public void should_map_to_response() throws Exception {
        //given
        Response response = new Response();

        //when
        responseMapper.mapToSuccessfulResponse(response);

        //then
        assertThat(response.getStatus()).isEqualTo(ResponseStatusType.SUCCESS.getValue());
        assertThat(response.getSystemTime()).isNotNull();
    }

    @Test
    public void should_create_successful_response() throws Exception {
        //when
        Response response = responseMapper.createSuccessfulResponse();

        //then
        assertThat(response.getStatus()).isEqualTo(ResponseStatusType.SUCCESS.getValue());
        assertThat(response.getSystemTime()).isNotNull();
    }
}