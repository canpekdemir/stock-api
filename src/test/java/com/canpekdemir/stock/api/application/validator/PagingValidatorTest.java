package com.canpekdemir.stock.api.application.validator;

import com.canpekdemir.stock.api.application.exception.StockApiValidationException;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

public class PagingValidatorTest {

    PagingValidator pagingValidator;

    @Before
    public void setUp() {
        pagingValidator = new PagingValidator();
    }

    @Test
    public void should_throw_exception_when_page_is_less_than_zero() throws Exception {
        //given
        Integer page = -5;
        Integer count = 10;

        //when
        Throwable throwable = catchThrowable(() -> pagingValidator.validate(page, count));

        //then
        assertThat(throwable).isInstanceOf(StockApiValidationException.class);
        StockApiValidationException stockApiValidationException = (StockApiValidationException) throwable;
        assertThat(stockApiValidationException).hasMessage("common.validation.field.minLength.genericMessage");
    }

    @Test
    public void should_throw_exception_when_count_is_less_than_zero() throws Exception {
        //given
        Integer page = 1;
        Integer count = -10;

        //when
        Throwable throwable = catchThrowable(() -> pagingValidator.validate(page, count));

        //then
        assertThat(throwable).isInstanceOf(StockApiValidationException.class);
        StockApiValidationException stockApiValidationException = (StockApiValidationException) throwable;
        assertThat(stockApiValidationException).hasMessage("common.validation.field.integerValue.invalidRange");
    }

    @Test
    public void should_throw_exception_when_count_is_greater_than_max_page_limit() throws Exception {
        //given
        Integer page = 1;
        Integer count = 1000;

        //when
        Throwable throwable = catchThrowable(() -> pagingValidator.validate(page, count));

        //then
        assertThat(throwable).isInstanceOf(StockApiValidationException.class);
        StockApiValidationException stockApiValidationException = (StockApiValidationException) throwable;
        assertThat(stockApiValidationException).hasMessage("common.validation.field.integerValue.invalidRange");
    }

    @Test
    public void should_validate_successfully() throws Exception {
        //given
        Integer page = 1;
        Integer count = 50;

        //when
        Throwable throwable = catchThrowable(() -> pagingValidator.validate(page, count));

        //then
        assertThat(throwable).isNull();
    }
}