package com.canpekdemir.stock.api.application.controller;

import com.canpekdemir.stock.api.application.model.request.StockRequest;
import com.canpekdemir.stock.api.application.model.response.Response;
import com.canpekdemir.stock.api.application.model.response.StockListResponse;
import com.canpekdemir.stock.api.application.model.response.StockResponse;

public interface StockController {

    StockListResponse retrieveStocks(Integer page, Integer count);

    StockResponse retrieveStock(Long stockId);

    Response createStock(StockRequest stockRequest);

    Response updateStock(Long stockId, StockRequest stockRequest);
}
