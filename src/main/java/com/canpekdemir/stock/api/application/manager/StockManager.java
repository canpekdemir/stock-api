package com.canpekdemir.stock.api.application.manager;

import com.canpekdemir.stock.api.application.converter.StockResponseConverter;
import com.canpekdemir.stock.api.application.manager.mapper.ResponseMapper;
import com.canpekdemir.stock.api.application.model.request.StockRequest;
import com.canpekdemir.stock.api.application.model.response.Response;
import com.canpekdemir.stock.api.application.model.response.StockListResponse;
import com.canpekdemir.stock.api.application.model.response.StockResponse;
import com.canpekdemir.stock.api.application.validator.PagingValidator;
import com.canpekdemir.stock.api.domain.entity.Stock;
import com.canpekdemir.stock.api.domain.service.StockLockService;
import com.canpekdemir.stock.api.domain.service.StockService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StockManager {

    private final StockService stockService;
    private final StockLockService stockLockService;
    private final StockResponseConverter stockResponseConverter;
    private final PagingValidator pagingValidator;
    private final ResponseMapper responseMapper;

    public StockManager(StockService stockService, StockLockService stockLockService, StockResponseConverter stockResponseConverter,
                        PagingValidator pagingValidator, ResponseMapper responseMapper) {
        this.stockService = stockService;
        this.stockLockService = stockLockService;
        this.stockResponseConverter = stockResponseConverter;
        this.pagingValidator = pagingValidator;
        this.responseMapper = responseMapper;
    }

    public StockListResponse retrieveStocks(Integer page, Integer count) {
        pagingValidator.validate(page, count);
        List<Stock> stocks = stockService.retrieveStocks(page, count);
        return stockResponseConverter.convertStocks(stocks);
    }

    public StockResponse retrieveStock(Long stockId) {
        Stock stock = stockService.retrieveStock(stockId);
        return stockResponseConverter.convertStockToResponse(stock);
    }

    public Response createStock(StockRequest stockRequest) {
        stockLockService.lock(stockRequest.getName());
        stockService.createStock(stockRequest.getName(), stockRequest.getCurrentPrice());
        return responseMapper.createSuccessfulResponse();
    }

    public Response updateStock(Long stockId, StockRequest stockRequest) {
        stockService.updateStock(stockId, stockRequest.getName(), stockRequest.getCurrentPrice());
        return responseMapper.createSuccessfulResponse();
    }
}
