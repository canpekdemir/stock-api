package com.canpekdemir.stock.api.application.util;

import org.owasp.validator.html.AntiSamy;
import org.owasp.validator.html.CleanResults;
import org.owasp.validator.html.Policy;
import org.owasp.validator.html.PolicyException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class XssCheckUtils {

    private static final Logger logger = LoggerFactory.getLogger(XssCheckUtils.class);

    private static Policy policy;

    private XssCheckUtils() {
    }

    public static Policy getPolicy() throws PolicyException {
        if (policy == null) {
            policy = Policy.getInstance(XssCheckUtils.class.getResource("/antisamy-slashdot-1.4.4.xml"));
        }
        return policy;
    }

    public static String cleanInputParam(String param) {
        try {
            if (param == null) {
                return null;
            }
            final Policy policy = getPolicy();
            final AntiSamy as = new AntiSamy();
            CleanResults cleanResults = as.scan(param, policy);
            return cleanResults.getCleanHTML();
        } catch (Exception e) {
            logger.error("Caught an exception while clearing input", e);
        }
        return param;
    }
}