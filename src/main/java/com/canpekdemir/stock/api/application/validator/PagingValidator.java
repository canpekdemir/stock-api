package com.canpekdemir.stock.api.application.validator;

import com.canpekdemir.stock.api.application.exception.StockApiValidationException;
import com.canpekdemir.stock.api.domain.util.NumberComparatorUtils;
import org.springframework.stereotype.Component;

import static com.canpekdemir.stock.api.domain.constant.SearchConstants.MAX_PAGE_LIMIT;

@Component
public class PagingValidator {

    public void validate(Integer page, Integer count) {
        if (page != null && NumberComparatorUtils.isLessThanOrEqual(page, 0)) {
            throw new StockApiValidationException("common.validation.field.minLength.genericMessage", "page", 0);
        }
        if (count != null &&
                (NumberComparatorUtils.isLessThanOrEqual(count, 0)
                        || NumberComparatorUtils.isGreaterThan(count, MAX_PAGE_LIMIT))) {
            throw new StockApiValidationException("common.validation.field.integerValue.invalidRange", "count", 0, MAX_PAGE_LIMIT);
        }
    }
}
