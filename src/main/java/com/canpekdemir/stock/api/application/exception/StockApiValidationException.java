package com.canpekdemir.stock.api.application.exception;

public class StockApiValidationException extends RuntimeException {

    private final Object[] arguments;

    public StockApiValidationException(String message) {
        super(message);
        arguments = new Object[0];
    }

    public StockApiValidationException(String message, Object... arguments) {
        super(message);
        this.arguments = arguments;
    }

    public Object[] getArguments() {
        return arguments;
    }
}