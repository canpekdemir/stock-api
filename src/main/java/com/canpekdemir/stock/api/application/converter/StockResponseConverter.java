package com.canpekdemir.stock.api.application.converter;

import com.canpekdemir.stock.api.application.manager.mapper.ResponseMapper;
import com.canpekdemir.stock.api.application.model.dto.StockDto;
import com.canpekdemir.stock.api.application.model.response.StockListResponse;
import com.canpekdemir.stock.api.application.model.response.StockResponse;
import com.canpekdemir.stock.api.application.util.XssCheckUtils;
import com.canpekdemir.stock.api.domain.entity.Stock;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class StockResponseConverter {

    private final ResponseMapper responseMapper;

    public StockResponseConverter(ResponseMapper responseMapper) {
        this.responseMapper = responseMapper;
    }

    public StockListResponse convertStocks(List<Stock> stocks) {
        List<StockDto> stockDtoList = stocks.stream().map(this::convertStockToDto).collect(Collectors.toList());

        StockListResponse response = new StockListResponse();
        response.setStocks(stockDtoList);
        responseMapper.mapToSuccessfulResponse(response);

        return response;
    }

    private StockDto convertStockToDto(Stock stock) {
        StockDto stockDto = new StockDto();
        stockDto.setId(stock.getId());
        stockDto.setName(XssCheckUtils.cleanInputParam(stock.getName()));
        stockDto.setCurrentPrice(stock.getCurrentPrice());
        return stockDto;
    }

    public StockResponse convertStockToResponse(Stock stock) {
        StockDto stockDto = convertStockToDto(stock);

        StockResponse response = new StockResponse();
        response.setStock(stockDto);
        responseMapper.mapToSuccessfulResponse(response);

        return response;
    }
}
