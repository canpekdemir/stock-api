package com.canpekdemir.stock.api.application.manager.mapper;

import com.canpekdemir.stock.api.application.model.response.Response;
import com.canpekdemir.stock.api.application.model.response.ResponseStatusType;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class ResponseMapper {

    public Response mapToSuccessfulResponse(Response response) {
        response.setStatus(ResponseStatusType.SUCCESS.getValue());
        response.setSystemTime(new Date().getTime());
        return response;
    }

    public Response createSuccessfulResponse() {
        Response response = new Response();
        response.setStatus(ResponseStatusType.SUCCESS.getValue());
        response.setSystemTime(new Date().getTime());
        return response;
    }
}
