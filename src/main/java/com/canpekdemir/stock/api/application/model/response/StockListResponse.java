package com.canpekdemir.stock.api.application.model.response;

import com.canpekdemir.stock.api.application.model.dto.StockDto;

import java.util.List;

public class StockListResponse extends Response {

    private List<StockDto> stocks;

    public List<StockDto> getStocks() {
        return stocks;
    }

    public void setStocks(List<StockDto> stocks) {
        this.stocks = stocks;
    }
}
