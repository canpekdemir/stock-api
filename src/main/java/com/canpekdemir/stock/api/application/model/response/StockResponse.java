package com.canpekdemir.stock.api.application.model.response;

import com.canpekdemir.stock.api.application.model.dto.StockDto;

public class StockResponse extends Response {

    private StockDto stock;

    public StockDto getStock() {
        return stock;
    }

    public void setStock(StockDto stock) {
        this.stock = stock;
    }
}
