package com.canpekdemir.stock.api.domain.repository;

import com.canpekdemir.stock.api.domain.entity.Stock;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface StockRepository extends JpaRepository<Stock, Long> {

    Optional<Stock> findById(Long stockId);

    Optional<Stock> findByName(String name);
}
