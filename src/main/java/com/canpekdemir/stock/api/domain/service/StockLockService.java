package com.canpekdemir.stock.api.domain.service;

import com.canpekdemir.stock.api.domain.exception.LockedException;
import com.canpekdemir.stock.api.domain.repository.LockRepository;
import com.canpekdemir.stock.api.domain.util.NumberComparatorUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
public class StockLockService {

    private static final Long LOCK_TTL = 5L;
    private static final String STOCK_PREFIX = "stock";
    private static final String KEY_SEPARATOR = ":";

    private final LockRepository lockRepository;

    public StockLockService(LockRepository lockRepository) {
        this.lockRepository = lockRepository;
    }

    public void lock(String name) {
        if (StringUtils.isNotBlank(name)) {
            Integer value = lockRepository.increment(generateLockKey(name), LOCK_TTL, TimeUnit.SECONDS);

            if (!NumberComparatorUtils.isEquals(1, value)) {
                throw new LockedException();
            }
        }
    }

    private static String generateLockKey(String name) {
        StringBuilder builder = new StringBuilder(STOCK_PREFIX);
        builder.append(KEY_SEPARATOR);
        builder.append(name);
        return builder.toString();
    }
}
