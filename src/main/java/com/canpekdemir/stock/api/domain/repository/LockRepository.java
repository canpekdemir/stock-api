package com.canpekdemir.stock.api.domain.repository;

import java.util.concurrent.TimeUnit;

public interface LockRepository {

    Integer increment(String key, Long timeout, TimeUnit timeUnit);
}