package com.canpekdemir.stock.api.domain.exception;

public class StockAlreadyCreatedException extends RuntimeException {

    public StockAlreadyCreatedException() {
        super("stock.error.alreadyCreated");
    }
}
