package com.canpekdemir.stock.api.domain.exception;

public class LockedException extends RuntimeException {

    public LockedException() {
        super("Process has been locked before");
    }
}