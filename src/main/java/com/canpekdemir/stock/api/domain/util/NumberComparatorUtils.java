package com.canpekdemir.stock.api.domain.util;


public class NumberComparatorUtils {

    public static boolean isLessThanOrEqual(Integer val1, Integer val2) {
        return (val1 != null && val2 != null) && val1.compareTo(val2) <= 0;
    }

    public static boolean isGreaterThan(Integer val1, Integer val2) {
        return (val1 != null && val2 != null) && val1.compareTo(val2) == 1;
    }

    public static boolean isEquals(Integer val1, Integer val2) {
        return val1 != null && val1.intValue() == val2;
    }
}
