package com.canpekdemir.stock.api.domain.constant;

public class SearchConstants {

    public static final int MAX_PAGE_LIMIT = 100;
    public static final int FIRST_PAGE_INDEX = 1;
}
