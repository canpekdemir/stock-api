package com.canpekdemir.stock.api.domain.exception;

public class StockNotFoundException extends RuntimeException {

    public StockNotFoundException() {
        super("stock.error.notFound");
    }
}
