package com.canpekdemir.stock.api.domain.service;

import com.canpekdemir.stock.api.domain.constant.SearchConstants;
import com.canpekdemir.stock.api.domain.entity.Stock;
import com.canpekdemir.stock.api.domain.exception.StockAlreadyCreatedException;
import com.canpekdemir.stock.api.domain.exception.StockNotFoundException;
import com.canpekdemir.stock.api.domain.repository.StockRepository;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.PageRequest;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class StockService {

    private final StockRepository stockRepository;

    public StockService(StockRepository stockRepository) {
        this.stockRepository = stockRepository;
    }

    @HystrixCommand(fallbackMethod = "reliableStockList")
    @Cacheable(value = "stocks")
    public List<Stock> retrieveStocks(Integer page, Integer count) {
        if (page == null) {
            page = SearchConstants.FIRST_PAGE_INDEX;
        }
        if (count == null) {
            count = SearchConstants.MAX_PAGE_LIMIT;
        }
        PageRequest pageRequest = new PageRequest(page - 1, count);
        return stockRepository.findAll(pageRequest).getContent();
    }

    //this method can be used by hystrix after opening the circuit to return stale data when db or cache is not working
    public List<Stock> reliableStockList() {
        List<Stock> stocks = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Stock stock = new Stock();
            stock.setName("STOCK" + i);
            stock.setCreationTime(new Date());
            stock.setCurrentPrice(new BigDecimal("0"));
            stocks.add(stock);
        }
        return stocks;
    }

    @Cacheable(value = "stock-single", key = "#stockId")
    public Stock retrieveStock(Long stockId) {
        Optional<Stock> stock = stockRepository.findById(stockId);
        return stock.orElseThrow(StockNotFoundException::new);
    }

    @Retryable(
            maxAttempts = 3,
            backoff = @Backoff(delay = 2000))
    @CacheEvict(value = {"stocks"})
    public void createStock(String name, BigDecimal currentPrice) {
        validateStockCreatedBefore(name);
        insertNewStock(name, currentPrice);
    }

    private void validateStockCreatedBefore(String name) {
        Optional<Stock> stockOptional = stockRepository.findByName(name);
        if (stockOptional.isPresent()) {
            throw new StockAlreadyCreatedException();
        }
    }

    private void insertNewStock(String name, BigDecimal currentPrice) {
        Stock stock = new Stock();
        stock.setName(name);
        stock.setCurrentPrice(currentPrice);
        stock.setCreationTime(new Date());
        stockRepository.save(stock);
    }

    @CachePut(value = {"stock-single"}, key = "#stockId")
    public void updateStock(Long stockId, String name, BigDecimal currentPrice) {
        validateStockCreatedBefore(name);

        Stock stock = retrieveStock(stockId);
        stock.setName(name);
        stock.setCurrentPrice(currentPrice);
        stock.setCreationTime(new Date());
        stockRepository.save(stock);
    }
}
