package com.canpekdemir.stock.api.infrastructure.adapter.persistence.cache;

import com.canpekdemir.stock.api.domain.repository.LockRepository;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.support.atomic.RedisAtomicInteger;
import org.springframework.stereotype.Repository;

import java.util.concurrent.TimeUnit;

@Repository
public class RedisLockRepository implements LockRepository {

    private final RedisTemplate<String, String> redisTemplate;

    public RedisLockRepository(RedisTemplate<String, String> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @Override
    public Integer increment(String key, Long timeout, TimeUnit timeUnit) {
        RedisAtomicInteger redisAtomicInteger = new RedisAtomicInteger(key, redisTemplate.getConnectionFactory());
        redisAtomicInteger.expire(timeout, timeUnit);
        return redisAtomicInteger.incrementAndGet();
    }
}