package com.canpekdemir.stock.api.infrastructure.rest;

import com.canpekdemir.stock.api.application.controller.StockController;
import com.canpekdemir.stock.api.application.manager.StockManager;
import com.canpekdemir.stock.api.application.model.request.StockRequest;
import com.canpekdemir.stock.api.application.model.response.Response;
import com.canpekdemir.stock.api.application.model.response.StockListResponse;
import com.canpekdemir.stock.api.application.model.response.StockResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class RestStockController implements StockController {

    private final StockManager stockManager;

    public RestStockController(StockManager stockManager) {
        this.stockManager = stockManager;
    }

    @Override
    @GetMapping(value = "/api/v1/stocks")
    @ResponseStatus(HttpStatus.OK)
    public StockListResponse retrieveStocks(@RequestParam(value = "page", required = false) Integer page,
                                            @RequestParam(value = "count", required = false) Integer count) {
        return stockManager.retrieveStocks(page, count);
    }

    @Override
    @GetMapping(value = "/api/v1/stocks/{stockId}")
    @ResponseStatus(HttpStatus.OK)
    public StockResponse retrieveStock(@PathVariable Long stockId) {
        return stockManager.retrieveStock(stockId);
    }

    @Override
    @PostMapping(value = "/api/v1/stocks")
    @ResponseStatus(HttpStatus.CREATED)
    public Response createStock(@Valid @RequestBody StockRequest stockRequest) {
        return stockManager.createStock(stockRequest);
    }

    @Override
    @PutMapping(value = "/api/v1/stocks/{stockId}")
    @ResponseStatus(HttpStatus.OK)
    public Response updateStock(@PathVariable Long stockId, @Valid @RequestBody StockRequest stockRequest) {
        return stockManager.updateStock(stockId, stockRequest);
    }
}
