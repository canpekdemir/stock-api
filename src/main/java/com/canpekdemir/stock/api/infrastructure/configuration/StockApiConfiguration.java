package com.canpekdemir.stock.api.infrastructure.configuration;

import com.canpekdemir.stock.api.domain.repository.StockRepository;
import com.canpekdemir.stock.api.infrastructure.interceptor.CorrelationIdInterceptor;
import com.canpekdemir.stock.api.infrastructure.interceptor.LogExecutionInterceptor;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@EnableJpaRepositories("com.canpekdemir.stock.api.domain.repository")
@EntityScan(basePackages = {"com.canpekdemir.stock.api.domain.entity"})
@EnableCaching
@EnableRetry
public class StockApiConfiguration {

    @Autowired
    StockRepository stockRepository;

    @Bean
    @Primary
    public Jackson2ObjectMapperBuilder objectMapperBuilder() {
        Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
        builder.serializationInclusion(JsonInclude.Include.NON_NULL);
        return builder;
    }

    @Bean
    public WebMvcConfigurerAdapter adapter() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addInterceptors(InterceptorRegistry registry) {
                registry.addInterceptor(new LogExecutionInterceptor());
                registry.addInterceptor(new CorrelationIdInterceptor());
            }
        };
    }
}
